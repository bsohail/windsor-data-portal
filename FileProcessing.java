import java.util.Scanner;
import java.io.*;

class FileProcessing {

    File processFile() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter absolute path to the file");
        String inputFileName = sc.nextLine().trim();
        File input = new File(inputFileName);
        sc.close();
        return input;
    }
}
