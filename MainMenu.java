import java.util.Scanner;
import java.io.*;

class MainMenu {

  public void readUserData() {
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter Name");
    String name = sc.nextLine();
    System.out.println("Enter Email");
    String email = sc.nextLine();
    System.out.println("Enter Company Name");
    String companyName = sc.nextLine();
    System.out.println("Enter Description");
    String description = sc.nextLine();
    FileProcessing readFile = new FileProcessing();
    File userFile = readFile.processFile();
    DataProcessing processData = new DataProcessing();
    processData.processData(name, email, companyName, description, userFile);

    sc.close();
  }
}